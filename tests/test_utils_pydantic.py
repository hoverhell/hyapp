from frozendict import frozendict
from pydantic import BaseModel

from hyapp.pydantics import PydanticDeepFrozenDict


def test_pydantic_frozen_dict() -> None:
    class Example(BaseModel):
        mapping: PydanticDeepFrozenDict[str, frozendict[str, int]]

    value = {"0": {"1": 2}}
    obj = Example(mapping=value)  # type: ignore[arg-type]
    assert isinstance(obj.mapping, frozendict)
    assert obj.mapping == value
    assert isinstance(obj.mapping["0"], frozendict)

    assert obj.model_dump() == {"mapping": value}
    json = obj.model_dump_json()
    loaded = Example.model_validate_json(json)
    assert isinstance(loaded.mapping, frozendict)
    assert loaded.mapping == value
    assert isinstance(loaded.mapping["0"], frozendict)
